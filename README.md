## About My Project

Lumen Project API <b>(G Digital)</b>

<br/><b>Desafio:</b><br/>
<br/>Lumen API:✓<br/>
<br/>Web Route:✓<br/>
<br/>Core:✓<br/> 
<br/>Migrations:✓<br/>
<br/>Upload My Repo on GitLab✓<br/>
<br/><b>Em Desenvolvimento</b><br/>
<br/>Landing Page with bootstrap layout:✖<br/>
<br/>Form Login & Sign Up:✖<br/>
<br/>Ajax Process:✖<br/>
<br/>Additonal Features, Notes:✖<br/> 


<strong>Comando para iniciar o server no Terminal direto da ide VSC: <b>php -S localhost:8000</b></strong>

Você pode ver em funcionamento aqui: <a href="http://ip-104-237-139-215.cloudezapp.io/">Lumen Project</a>



## License

The Lumen & Laravel frameworks are open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

## Dependencies

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](http://lumen.laravel.com/docs).


